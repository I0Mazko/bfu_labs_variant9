#include "iostream"
#include "cmath"
#include "../inc/Functions.hpp"
int main(int argc, char const *argv[]) 
{
  setlocale(LC_ALL, "russian");
  int n,m;
  std::cout << "Vvedite i,j" << '\n';
  std::cin >> n >> m;
  int** A = new int*[n];
  for (size_t i = 0; i < n; i++) 
  {
    A[i] = new int[m];
  }
  mf::cin_matrix(A, n, m);
  if (fabs(mf::sum_number(mf::Max_Number(A, n, m)) - mf::sum_number(mf::Min_Number(A, n, m))) <= 2)
  {
    int* sums = mf::sum_rows(A, n, m);
    mf::sort_matrix(A, sums, n, m);
    mf::cout_matrix(A, n, m);
    delete[] sums;
  }
  else
  {
    std::cout << "Sum of Max - Sum of Min > 2" << '\n';
  }
  for (size_t i = 0; i < n; i++) 
  {
    delete[] A[i];
  }
  delete[] A;
  
  return 0;
}
