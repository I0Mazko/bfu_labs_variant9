#pragma once
namespace mf
{
	void cin_matrix(int** A, int N, int M);
	void cout_matrix(int** A, int N, int M);
	int Max_Number(int** A, int N, int M);
	int Min_Number(int** A, int N, int M);
	int sum_number(int number);
	int* sum_rows(int** A, int N, int M);
	void swap_rows(int** A, int left, int right, int N);
	void sort_matrix(int** A, int* sums, int N, int M);
}