#include "iostream"
#include "Functions.hpp"
namespace mf
{

    void cin_matrix(int** A, int N, int M)
    {
        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < M; j++)
            {
                std::cin >> A[i][j];
            }
        }
        std::cout << '\n';
    }
    void cout_matrix(int** A, int N, int M)
    {
        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < M; j++)
            {
                std::cout << A[i][j] << '\n';
            }
            std::cout << std::endl;
        }
    }
    int Max_Number(int** A, int N, int M)
    {
        int Max = A[0][0];
        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < M; j++)
            {
                if (A[i][j] > Max)
                {
                    Max = A[i][j];
                }
            }
        }
        return Max;
    }
    int Min_Number(int** A, int N, int M)
    {
        int Min = A[0][0];
        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < M; j++)
            {
                if (A[i][j] < Min)
                {
                    Min = A[i][j];
                }
            }
        }
        return Min;
    }
    int sum_number(int number)
    {
        int sum = 0;
        while (number > 0)
        {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }
    int* sum_rows(int** A, int N, int M) //создание массива суммы столбцов
    {
        int* sums = new int[N];
        for (size_t i = 0; i < N; i++)
        {
            sums[i] = 0;
            for (size_t j = 0; j < M; j++)
            {
                sums[i] += A[j][i];
            }
        }
        return sums;
    }
    void swap_rows(int** A, int left, int right, int N) //меняем стобцы местами (например правый элемент - 2-ой столбец, а левый столбец - 1-ый)
    {
        for (size_t j = 0; j < N; j++)
        {
            int temp = A[j][left];
            A[j][left] = A[j][right];
            A[j][right] = temp;
        }
    }
    void sort_matrix(int** A, int* sums, int N, int M)
    {
        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < M - i - 1; j++)
            {
                if (sums[j] > sums[j + 1])
                {
                    std::swap(sums[j], sums[j + 1]);
                    swap_rows(A, j, j + 1, N);
                }
            }
        }
    }
}